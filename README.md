# ViUR Docker Containers

## tl;dr;
	
	docker pull viur/examplecms
	docker run -d -p 8000:8000 -p 8080:8080 --name viur-examplecms viur/examplecms

## provided dockerfiles

The Dockerfiles without suffix (debian based) are working out of the box, the Dockerfiles with alpine suffix have some segfaults right now.

## run your projects with ViUR Control

	docker run -d -v /path/to/your/projects:/Projects -p 5000:5000 -p 8000:8000 -p 8080:8080 --name viur-vpc viur/viur-vpc