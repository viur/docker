#!/bin/bash

docker build -t viur/viur-base viur-base && \
docker build -t viur/viur-env viur-env && \
docker build -t viur/viur-vpc viur-vpc && \
docker build -t viur/examplecms examplecms
